#include <math.h>
#include <ctype.h>
#include <bytenum.h>

__declspec(dllexport)
double bytenum_eval(char *str)
{
  double val = (double) fabs(atof(str));
  char power[] = "bkmgtp";
  char mod_char = tolower((str[strlen(str) - 1]));

  for(int index = 0; power[index] != '\0'; index++)
  {
    if(mod_char == power[index]) return ceil(val * pow(1024, index));
  }
  return ceil(val);
}

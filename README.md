bytenum is a small c libray that evaluates a string for a representation of bytes and returns the number of bytes rounded up to the nearest byte.
Example:
"1K" evaluates as 1024
"1M" evaluates as 1048576
"1.1M" evaluates as 1153434

Recognized symbols are: B K M G T P, for Bytes Kilobytes MegaBytes Gigabytes Terabyes and Petabytes.

See test/test.c for an example of usage.


#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <bytenum.h>

int main(int argc, char **argv)
{
  assert(argv[1]);

  printf("%f\n", bytenum_eval(argv[1]));
  
  return 0;
}

CC=gcc
CFLAGS=-Wall -s

export CC CFLAGS

main:
	$(CC) $(CFLAGS) -Ibytenum bytenum/bytenum.c test/test.c -o test.exe

all: libbytenum.a bytenum.dll

libbytenum.a:
	cd bytenum && $(MAKE) libbytenum.a

bytenum.dll:
	cd bytenum && $(MAKE) bytenum.dll

test_dll: bytenum.dll
	cd test && $(MAKE) test_dll

test_a: libbytenum.a
	cd test && $(MAKE) test_a

clean:
	cd bytenum && $(MAKE) clean
	cd test && $(MAKE) clean
	$(RM) test.exe libbytenum.a bytenum.dll bytenum.def
